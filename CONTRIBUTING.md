# Contributing

When contributing to this repository, please follow the Guidelines below.

# Guidelines to be followed
* [PEP 8](https://pep8.org/)
* [GNOME HIG](https://developer.gnome.org/hig/)


## Merge Request Process

1. Ensure that any artifacts are ignored in **`.gitignore`**.
2. Make sure that **`flake8 --ignore E402,W503 saldo`** runs without warning.
3. Build and test at least with **`flatpak-builder`**.
4. Update the README.md when necessary.
5. Have the MR reviewed by @jbrummer.
6. Optionally, if the changes pertain to the backend as well, <br>
   then you can use a checklist to collect feedback from users, <br>
   who can test with banks that you personally aren't able to test with. <br>
   For example:

* [ ] Sparkasse X
* [ ] Volksbank Y
* [ ] Comdirect
* [ ] ...
