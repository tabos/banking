# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Adw, Gtk


@Gtk.Template(resource_path="/org/tabos/saldo/ui/mechanism_dialog.ui")
class MechanismDialog(Adw.AlertDialog):
    __gtype_name__ = "MechanismDialog"

    _listbox = Gtk.Template.Child()

    def __init__(self, mechanisms, **kwargs):
        super().__init__(**kwargs)

        self.mechanism = 0

        group = None

        for i, mechanism in enumerate(mechanisms):
            row = Adw.ActionRow()
            row.set_title(mechanism[1].name)

            check_box = Gtk.CheckButton()
            check_box.set_valign(Gtk.Align.CENTER)
            check_box.connect("toggled", self._on_radio_button_toggled)
            row.set_activatable_widget(check_box)

            check_box.set_group(group)
            if group is None:
                group = check_box

            row.add_prefix(check_box)

            setattr(row, "id", i)

            self._listbox.append(row)

    def _on_radio_button_toggled(self, button: Gtk.Widget) -> None:
        if button.get_active():
            row = button.get_ancestor(Adw.ActionRow)
            idx = getattr(row, "id")
            self.mechanism = idx
