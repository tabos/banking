import time
from saldo.backend.account_data import AccountData


def init_demo_bank(self, client):
    account_info = {}
    account_info["logo"] = "dsgv"
    account_info["bank_name"] = "Demo Bank"
    account_info["product_name"] = "Giro"
    account_info["currency"] = "EUR"
    account_info["last_updated"] = time.time()
    account_info["iban"] = "340027267"
    account_info["subaccount_number"] = ""
    account_info["owner_name"] = "Max Mustermann"
    account_info["balance"] = 38013.54

    account_id = self.add_account(client, account_info)
    transaction_json = []

    transaction = {}
    transaction["status"] = "C"
    transaction["funds_code"] = ""
    transaction["amount"] = -920.00
    transaction["id"] = ""
    transaction["customer_reference"] = ""
    transaction["bank_reference"] = ""
    transaction["extra_details"] = ""
    transaction["currency"] = "EUR"
    transaction["date"] = "2013-01-02"
    transaction["entry_date"] = "2013-01-02"
    transaction["guessed_entry_date"] = "2013-01-02"
    transaction["transaction_code"] = ""
    transaction["posting_text"] = "MIETE JANUARY 2013"
    transaction["prima_nota"] = ""
    transaction["purpose"] = ""
    transaction["applicant_bin"] = ""
    transaction["applicant_iban"] = ""
    transaction["applicant_name"] = "Hausverwaltung Musterweg 2"
    transaction["return_debit_notes"] = ""
    transaction["recipient_name"] = ""
    transaction["additional_purpose"] = ""
    transaction["additional_position_reference"] = ""
    transaction["end_to_end_reference"] = ""
    transaction_json.append(transaction)

    transaction = {}
    transaction["status"] = "D"
    transaction["funds_code"] = ""
    transaction["amount"] = 2420.37
    transaction["id"] = ""
    transaction["customer_reference"] = ""
    transaction["bank_reference"] = ""
    transaction["extra_details"] = ""
    transaction["currency"] = "EUR"
    transaction["date"] = "2012-12-27"
    transaction["entry_date"] = "2012-12-27"
    transaction["guessed_entry_date"] = "2012-12-27"
    transaction["transaction_code"] = ""
    transaction["posting_text"] = "GEHALT 01 2013"
    transaction["prima_nota"] = ""
    transaction["purpose"] = ""
    transaction["applicant_bin"] = ""
    transaction["applicant_iban"] = ""
    transaction["applicant_name"] = "Max Mustermann GmbH"
    transaction["return_debit_notes"] = ""
    transaction["recipient_name"] = ""
    transaction["additional_purpose"] = ""
    transaction["additional_position_reference"] = ""
    transaction["end_to_end_reference"] = ""
    transaction_json.append(transaction)

    transaction = {}
    transaction["status"] = "C"
    transaction["funds_code"] = ""
    transaction["amount"] = -9.79
    transaction["id"] = ""
    transaction["customer_reference"] = ""
    transaction["bank_reference"] = ""
    transaction["extra_details"] = ""
    transaction["currency"] = "EUR"
    transaction["date"] = "2012-12-26"
    transaction["entry_date"] = "2012-12-26"
    transaction["guessed_entry_date"] = "2012-12-26"
    transaction["transaction_code"] = ""
    transaction["posting_text"] = "SIEHE ANLAGE"
    transaction["prima_nota"] = ""
    transaction["purpose"] = ""
    transaction["applicant_bin"] = ""
    transaction["applicant_iban"] = ""
    transaction["applicant_name"] = "Entgeltabrechnung"
    transaction["return_debit_notes"] = ""
    transaction["recipient_name"] = ""
    transaction["additional_purpose"] = ""
    transaction["additional_position_reference"] = ""
    transaction["end_to_end_reference"] = ""
    transaction_json.append(transaction)

    transaction = {}
    transaction["status"] = "C"
    transaction["funds_code"] = ""
    transaction["amount"] = -123.00
    transaction["id"] = ""
    transaction["customer_reference"] = ""
    transaction["bank_reference"] = ""
    transaction["extra_details"] = ""
    transaction["currency"] = "EUR"
    transaction["date"] = "2012-12-23"
    transaction["entry_date"] = "2012-12-23"
    transaction["guessed_entry_date"] = "2012-12-23"
    transaction["transaction_code"] = ""
    transaction["posting_text"] = "ELV466776534678"
    transaction["prima_nota"] = ""
    transaction["purpose"] = ""
    transaction["applicant_bin"] = ""
    transaction["applicant_iban"] = ""
    transaction["applicant_name"] = "KFZ Versicherung GmbH"
    transaction["return_debit_notes"] = ""
    transaction["recipient_name"] = ""
    transaction["additional_purpose"] = ""
    transaction["additional_position_reference"] = ""
    transaction["end_to_end_reference"] = ""
    transaction_json.append(transaction)

    transaction = {}
    transaction["status"] = "D"
    transaction["funds_code"] = ""
    transaction["amount"] = 216.15
    transaction["id"] = ""
    transaction["customer_reference"] = ""
    transaction["bank_reference"] = ""
    transaction["extra_details"] = ""
    transaction["currency"] = "EUR"
    transaction["date"] = "2012-12-21"
    transaction["entry_date"] = "2012-12-21"
    transaction["guessed_entry_date"] = "2012-12-21"
    transaction["transaction_code"] = ""
    transaction["posting_text"] = "RUECKERSTATTUNG KD.-NR. 123456"
    transaction["prima_nota"] = ""
    transaction["purpose"] = ""
    transaction["applicant_bin"] = ""
    transaction["applicant_iban"] = ""
    transaction["applicant_name"] = "Vertragkuendigung"
    transaction["return_debit_notes"] = ""
    transaction["recipient_name"] = ""
    transaction["additional_purpose"] = ""
    transaction["additional_position_reference"] = ""
    transaction["end_to_end_reference"] = ""
    transaction_json.append(transaction)

    transaction = {}
    transaction["status"] = "C"
    transaction["funds_code"] = ""
    transaction["amount"] = -75.00
    transaction["id"] = ""
    transaction["customer_reference"] = ""
    transaction["bank_reference"] = ""
    transaction["extra_details"] = ""
    transaction["currency"] = "EUR"
    transaction["date"] = "2012-12-21"
    transaction["entry_date"] = "2012-12-21"
    transaction["guessed_entry_date"] = "2012-12-21"
    transaction["transaction_code"] = ""
    transaction["posting_text"] = "AUTOMAT 12345678 LINDENALLEE"
    transaction["prima_nota"] = ""
    transaction["purpose"] = ""
    transaction["applicant_bin"] = ""
    transaction["applicant_iban"] = ""
    transaction["applicant_name"] = "Barauszahlung"
    transaction["return_debit_notes"] = ""
    transaction["recipient_name"] = ""
    transaction["additional_purpose"] = ""
    transaction["additional_position_reference"] = ""
    transaction["end_to_end_reference"] = ""
    transaction_json.append(transaction)

    if account_id >= 0:
        self.add_transactions(account_id, transaction_json)

    account = {}
    account["transactions"] = transaction_json
    account_data = AccountData(account)
    self._accounts.append(account_data)

    account_info = {}
    account_info["logo"] = "bank"
    account_info["bank_name"] = "Demo Bank"
    account_info["product_name"] = "KapitalKonto"
    account_info["currency"] = "EUR"
    account_info["last_updated"] = time.time()
    account_info["iban"] = "DE111234567800000002"
    account_info["subaccount_number"] = ""
    account_info["owner_name"] = "Jane Doe"
    account_info["balance"] = 321.00

    account_id = self.add_account(client, account_info)

    transaction_json = []
    if account_id >= 0:
        self.add_transactions(account_id, transaction_json)

    account = {}
    account["transactions"] = transaction_json
    account_data = AccountData(account)
    self._accounts.append(account_data)
