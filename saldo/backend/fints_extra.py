# This module contains extra functionality
# which is currently not upstreamed to python-fints.
# Based on PR, like: https://github.com/raphaelm/python-fints/pull/80
# Considering https://github.com/raphaelm/python-fints/#maintenance-status
# it appears unlikely that PR 80 will be merged,
# but in case that happens, then please remove the code that became
# available upstream.


# Portions Copyright 2019 Henryk Plötz
# Portions Copyright 2021 Ferenc Géczi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from fints.formals import (
    Balance1,
    CodeField,
    CreditDebit2,
    DataElementField,
    DataElementGroup,
    DataElementGroupField,
)

from fints.segments.base import FinTS3Segment


class CreditCardTransaction1(DataElementGroup):
    """Kreditkartenumsatz"""

    credit_card_number = DataElementField(type="an", _d="Kreditkartennummer")
    receipt_date = DataElementField(type="dat", _d="Belegdatum")
    booking_date = DataElementField(type="dat", _d="Buchungsatum")
    value_date = DataElementField(type="dat", _d="Wertstellungsdatum")
    original_amount = DataElementField(type="wrt", _d="Original-Wert")
    currency = DataElementField(type="cur", _d="Währung")
    credit_debit = CodeField(enum=CreditDebit2, length=1, _d="Soll-Haben-Kennzeichen")
    exchange_rate = DataElementField(type="float", _d="Umrechnungskurs")
    booked_amount = DataElementField(type="wrt", _d="Gebuchter Wert")
    booked_currency = DataElementField(type="cur", _d="Gebuchte Währung")
    booked_credit_debit = CodeField(
        enum=CreditDebit2, length=1, _d="Gebuchtes Soll-Haben-Kennzeichen"
    )
    memo = DataElementField(type="an", _d="Buchungstext", count=9)
    settled = DataElementField(type="jn")
    booking_reference = DataElementField(type="an", _d="Buchungsreferenz")


class DIKKU2(FinTS3Segment):
    """Kreditkartenumsätze rückmelden, version 2"""

    credit_card_number = DataElementField(type="an", _d="Kreditkartennummer")
    _unknown_1 = DataElementField(type="an")
    balance = DataElementGroupField(type=Balance1, _d="Saldo")
    _unknown_2 = DataElementField(type="an", required=False)
    _unknown_3 = DataElementField(type="an", required=False)
    transactions = DataElementGroupField(
        type=CreditCardTransaction1,
        _d="Kreditkartenumsatz",
        min_count=1,
        required=False,
    )
