blueprints = custom_target('blueprints',
  input: files(
    'ui/welcome_page.blp',
    'ui/help-overlay.blp',
    'ui/settings_dialog.blp',
    'ui/locked_page.blp',
    'ui/window.blp',
    'ui/clients.blp',
    'ui/account_row.blp',
    'ui/create_safe_page.blp',
    'ui/change_password_dialog.blp',
    'ui/unlocked_page.blp',
    'ui/transaction_details.blp',
  ),
  output: '.',
  command: [find_program('blueprint-compiler'), 'batch-compile', '@OUTPUT@', '@CURRENT_SOURCE_DIR@', '@INPUT@'],
)

# Compiling the resources
gnome.compile_resources(
  APPLICATION_ID,
  APPLICATION_ID + '.gresource.xml',
  gresource_bundle: true,
  source_dir: meson.current_build_dir(),
  install_dir: PKGDATA_DIR,
  install: true,
  dependencies: [about_dialog, blueprints]
)

# Installing the schema file
install_data(
  APPLICATION_ID + '.gschema.xml',
  install_dir: join_paths(get_option('datadir'), 'glib-2.0/schemas')
)

# Merging the translations with the desktop file
desktop_conf = configuration_data()
desktop_conf.set('icon', APPLICATION_ID)
i18n.merge_file(
  type: 'desktop',
  input: configure_file(
    output: APPLICATION_ID + '.desktop.in',
    input: APPLICATION_ID + '.desktop.in.in',
    configuration: desktop_conf),
  output: APPLICATION_ID + '.desktop',
  po_dir: join_paths(meson.project_source_root(), 'po'),
  install: true,
  install_dir: join_paths(get_option('prefix'), get_option('datadir'), 'applications')
)

# Validating the desktop file
desktop_file_validate = find_program('desktop-file-validate', required:false)
if desktop_file_validate.found()
  test (
    'Validate desktop file',
    desktop_file_validate,
    args: join_paths(meson.current_build_dir (), APPLICATION_ID + '.desktop')
  )
endif

# Merging the translations with the appdata file
appdata_conf = configuration_data()
appdata_conf.set('appid', APPLICATION_ID)
appdata_conf.set('package_url', PACKAGE_URL)
appdata_conf.set('package_url_bug', PACKAGE_URL_BUG)
i18n.merge_file(
  input: configure_file(
    output: APPLICATION_ID + '.appdata.xml.in',
    input: APPLICATION_ID + '.appdata.xml.in.in',
    configuration: appdata_conf
  ),
  output: APPLICATION_ID + '.appdata.xml',
  po_dir: join_paths(meson.project_source_root(), 'po'),
  install: true,
  install_dir: join_paths(get_option('prefix'), get_option('datadir'), 'metainfo')
)

# Validating the appdata file
appstreamcli = find_program('appstreamcli', required: false)
if appstreamcli.found()
  test (
    'Validate appdata file',
    appstreamcli,
    args: ['validate', '--no-net', '--explain', join_paths(meson.current_build_dir (), APPLICATION_ID + '.appdata.xml')]
  )
endif

# Installing the default icon
install_data(
    join_paths('icons/hicolor/scalable/apps', APPLICATION_ID + '.svg'),
    install_dir: join_paths(get_option('prefix'), get_option('datadir'), 'icons/hicolor/scalable/apps')
)

# Installing the symbolic icon
install_data(
    join_paths('icons/hicolor/symbolic/apps', APPLICATION_ID + '-symbolic.svg'),
    install_dir: join_paths(get_option('prefix'), get_option('datadir'), 'icons/hicolor/symbolic/apps'),
    rename: '@0@-symbolic.svg'.format(APPLICATION_ID)
)
